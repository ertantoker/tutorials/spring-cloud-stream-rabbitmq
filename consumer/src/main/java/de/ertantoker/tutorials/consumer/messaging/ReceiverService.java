package de.ertantoker.tutorials.consumer.messaging;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.stereotype.Component;

@Component
public class ReceiverService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ReceiverService.class);

    @StreamListener("producerInput")
    public void handleMessage(String message) {
        LOGGER.info("Receive Message from RabbitMQ: {}", message);
    }
}
