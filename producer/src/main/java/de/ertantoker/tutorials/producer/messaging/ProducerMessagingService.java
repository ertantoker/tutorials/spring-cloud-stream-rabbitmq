package de.ertantoker.tutorials.producer.messaging;

import lombok.AllArgsConstructor;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class ProducerMessagingService {

    private final ProducerSource producerSource;

    public void sendMessage(String message) {
        producerSource.producerChannel().send(MessageBuilder.withPayload(message).build());
    }
}
