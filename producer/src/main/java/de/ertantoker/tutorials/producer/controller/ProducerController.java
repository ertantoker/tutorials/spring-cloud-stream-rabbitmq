package de.ertantoker.tutorials.producer.controller;

import de.ertantoker.tutorials.producer.messaging.ProducerMessagingService;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/producer")
@AllArgsConstructor
public class ProducerController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProducerController.class);

    private final ProducerMessagingService producerMessagingService;


    @PostMapping("/{message}")
    public void sendMessage(@PathVariable String message) {
        LOGGER.info("Receive message: {}", message);
        producerMessagingService.sendMessage(message);
    }
}
