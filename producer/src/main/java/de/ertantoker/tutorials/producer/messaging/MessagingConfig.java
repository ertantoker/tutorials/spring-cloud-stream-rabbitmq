package de.ertantoker.tutorials.producer.messaging;

import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableBinding({ProducerSource.class})
public class MessagingConfig {
}
