package de.ertantoker.tutorials.producer.messaging;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

public interface ProducerSource {

    @Output("producerOutput")
    MessageChannel producerChannel();
}
